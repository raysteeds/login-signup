import React from 'react';
import SocialLogin from 'react-social-login';

class GoogleLoginButton extends React.Component {
    render() {
        return(
            <button  className="cmn-btn" onClick={this.props.triggerLogin} {...this.props}>
                { this.props.children }
            </button>
        );
    }
}

export default SocialLogin(GoogleLoginButton);