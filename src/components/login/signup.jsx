import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';
import {MDBBtn, MDBCol, MDBContainer, MDBRow} from "mdbreact";

const Config = require('../../config/config');

export class Signup extends React.Component {
    constructor() {
        super();
        this.form = {
            name: '',
            email: '',
            password: ''
        };
        this.state = {
            form: {...this.form},
            message: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        let {form} = this.state;
        form = {...form};
        form[e.target.name] = e.target.value;
        this.setState({form});
    }

    async onSubmit(e) {
        e.preventDefault();
        const {form} = this.state;
        try {
            const result = await axios.post(`${Config.baseUrl}/api/user`, form);
            this.setState({ message: result.data });
        } catch (err) {
            const {response: {data}} = err;
            console.log('error', data);
        }
    }

    renderMessage() {
        const { message } = this.state;
        return (
            <div>
                {message}
            </div>
        );
    }

    render() {
        const {form: {name, email, password}} = this.state;
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6" className="mx-auto mt-5">
                        <form onSubmit={this.onSubmit}>
                            <p className="h4 text-center mb-4">Sign Up</p>
                            <label htmlFor="defaultFormLoginNameEx" className="grey-text">
                                Your name
                            </label>
                            <input
                                type="text"
                                id="defaultFormLoginNameEx"
                                className="form-control"
                                name="name"
                                value={name}
                                onChange={this.onChange}
                                required
                            />
                            <br />
                            <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                Your email
                            </label>
                            <input
                                type="email"
                                id="defaultFormLoginEmailEx"
                                className="form-control"
                                name="email"
                                value={email}
                                onChange={this.onChange}
                                required
                            />
                            <br />
                            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
                                Your password
                            </label>
                            <input
                                type="password"
                                id="defaultFormLoginPasswordEx"
                                className="form-control"
                                name="password"
                                value={password}
                                onChange={this.onChange}
                            />
                            <div className="text-center mt-4">
                                <MDBBtn color="indigo" type="submit">SignUp</MDBBtn>
                            </div>
                        </form>
                        {this.renderMessage()}
                        <div>
                            Already Registered,
                            <Link to="/" className="blue-text ml-1"> Login here!</Link>
                        </div>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
}