import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon } from 'mdbreact';
import {Link} from 'react-router-dom';
import axios from 'axios';
import GoogleLoginButton from './SocialLogin/googleLogin';

const Config = require('../../config/config');

export class Login extends React.Component {
    constructor() {
        super();
        this.form = {
            email: '',
            password: ''
        };
        this.state = {
            form: {...this.form},
            message: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        let { form } = this.state;
        form = { ...form };
        form[e.target.name] = e.target.value;
        this.setState({ form });
    }

    async onSubmit(e) {
        e.preventDefault();
        const { form } = this.state;
        const { history } = this.props;
        try {
            const result = await axios.post(`${Config.baseUrl}/api/login`, form);
            history.push('/dashboard');
        } catch (err) {
            const { response: { data } } = err;
            this.setState({ message: data.message });
        }
    }

    handleSocialLogin = async (user) => {
        const { profile } = user;
        const data = { name: profile.name, email: profile.email };
        try {
            const result = await axios.post(`${Config.baseUrl}/api/createSocial`, data);
            this.setState({ message: result.data });
        } catch(err) {
            const {response: {data}} = err;
            console.log('error', data);
        }
    }

    handleFacebookLogin = async (user) => {
        const { _profile } = user;
        const data = { name: _profile.name, email: _profile.email };
        try {
            const result = await axios.post(`${Config.baseUrl}/api/createSocial`, data);
            this.setState({ message: result.data });
        } catch(err) {
            const {response: {data}} = err;
            console.log('error', data);
        }
    }

    handleSocialLoginFailure = (err) => {
        console.error(err);
    }

    renderErrorMessage() {
        const { message } = this.state;
        return (
            <div>
                {message}
            </div>
        );
    }

    render() {
        const { form: { email, password } } = this.state;
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6" className="mx-auto mt-5">
                        <form onSubmit={this.onSubmit}>
                            <p className="h4 text-center mb-4">Sign in</p>
                            <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                Your email
                            </label>
                            <input
                                type="email"
                                id="defaultFormLoginEmailEx"
                                className="form-control"
                                name="email"
                                value={email}
                                onChange={this.onChange}
                                required
                            />
                            <br />
                            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
                                Your password
                            </label>
                            <input
                                type="password"
                                id="defaultFormLoginPasswordEx"
                                className="form-control"
                                name="password"
                                value={password}
                                onChange={this.onChange}
                            />
                            <div className="text-center mt-4">
                                <MDBBtn color="indigo" type="submit">Login</MDBBtn>
                            </div>
                        </form>
                        {this.renderErrorMessage()}
                        <div>
                            Not Registered Yet,
                            <Link to="/signup" className="blue-text ml-1"> SignUp here!</Link>
                        </div>
                        <div>
                            Or,<br />
                            <GoogleLoginButton
                                provider='google'
                                appId='168132237101-9sjlf9a6icdn7rn4bjivb08ms2ok2f4i.apps.googleusercontent.com'
                                onLoginSuccess={this.handleSocialLogin}
                                onLoginFailure={this.handleSocialLoginFailure}
                            >
                                <a href="#" className="google btn"><MDBIcon fab icon="google" /> &nbsp; Login with Google
                                </a>
                            </GoogleLoginButton>
                            &nbsp;&nbsp;
                            <GoogleLoginButton
                                provider='facebook'
                                appId='548069549102244'
                                onLoginSuccess={this.handleFacebookLogin}
                                onLoginFailure={this.handleSocialLoginFailure}
                            >
                                <a href="#" className="fb btn"><MDBIcon fab icon="facebook-f" /> &nbsp; Login with Facebook
                                </a>
                            </GoogleLoginButton>
                        </div>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
}